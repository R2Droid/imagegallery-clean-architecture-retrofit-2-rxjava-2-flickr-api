package com.droid.juice.sampleapprest.presentation;


import android.util.Log;

import com.droid.juice.sampleapprest.domain.entity.Photo;
import com.droid.juice.sampleapprest.domain.usecase.UseCaseFactory;
import com.droid.juice.sampleapprest.presentation.view.PhotoItemView;
import com.droid.juice.sampleapprest.presentation.view.PhotoListView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class PhotoListPresenter {

    private final UseCaseFactory useCaseFactory;
    private PhotoListView view;
    private List<Photo> photoList;

    public PhotoListPresenter(UseCaseFactory useCaseFactory) {
        this.useCaseFactory = useCaseFactory;
        photoList = new ArrayList<>();
    }

    public void onSearchClicked(String searchText) {
        photoList = new ArrayList<>();
        loadNextImagePage(searchText);
    }

    private void loadNextImagePage(String searchText) {
        if (!searchText.isEmpty()) {
            useCaseFactory.SearchPhotosUseCase(searchText)
                    .execute()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<List<Photo>>() {
                        @Override
                        public void onSuccess(List<Photo> photos) {
                            photoList.addAll(photos);
                            view.refreshList();
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.showListLoadErrorMessage();
                            Log.e("ListPresenter", e.getMessage());
                        }
                    });
        }
    }

    public void onViewCreate(PhotoListView view) {
        this.view = view;
    }

    public void configurePhotoItem(PhotoItemView itemView, int position) {
        itemView.setTitle(photoList.get(position).getTitle());
        itemView.setImage(photoList.get(position).getImage());
    }

    public int getPhotoListCount() {
        return photoList.size();
    }
}