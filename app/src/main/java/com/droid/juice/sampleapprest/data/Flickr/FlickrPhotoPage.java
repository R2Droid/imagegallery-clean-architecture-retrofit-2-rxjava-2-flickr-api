package com.droid.juice.sampleapprest.data.Flickr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

class FlickrPhotoPage {
    @SerializedName("page")
    @Expose
    private Integer page;

    @SerializedName("pages")
    @Expose
    private String pages;

    @SerializedName("perpage")
    @Expose
    private Integer perpage;

    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("photo")
    @Expose
    private List<FlickrPhoto> photoList;

    public List<FlickrPhoto> getPhotoList() {
        return photoList;
    }
}
