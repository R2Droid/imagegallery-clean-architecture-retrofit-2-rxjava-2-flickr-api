package com.droid.juice.sampleapprest.data.util;

import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class GlideImageUtil {
    private static final float sizeMultiplier = 0.4f;
    private static final RequestListener requestListener = new RequestListener() {
        @Override
        public boolean onException(Exception e, Object model, Target target, boolean isFirstResource) {
            Log.e("Glide: ", e.getMessage());
            return false;
        }

        @Override
        public boolean onResourceReady(Object resource, Object model, Target target, boolean isFromMemoryCache, boolean isFirstResource) {
            return false;
        }
    };

    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .asBitmap()
                .listener(requestListener)
                .thumbnail(sizeMultiplier)
                .centerCrop()
                .into(imageView);
    }
}
