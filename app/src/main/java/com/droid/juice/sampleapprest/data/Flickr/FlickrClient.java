package com.droid.juice.sampleapprest.data.Flickr;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

class FlickrClient {
    private static final String FARM = "farm";
    private static final String STATIC_FLICKR = "staticflickr.com";
    private static final String BASE_URL = "https://api.flickr.com/";
    private static final OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

    private final static Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    private static final Retrofit flickrClient = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();


    static Retrofit getFlickrClient() {
        return flickrClient;
    }

    static String createImageUrl(int farmId, String serverId, String imageId, String secret) {
        return "https://" +
                FARM + farmId + "." +
                STATIC_FLICKR + "/" +
                serverId + "/" +
                imageId + "_" +
                secret +
                "_m" +
                ".jpg";
    }
}
