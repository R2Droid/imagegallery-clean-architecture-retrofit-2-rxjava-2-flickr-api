package com.droid.juice.sampleapprest.android.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import com.droid.juice.sampleapprest.R;
import com.droid.juice.sampleapprest.presentation.PhotoListPresenter;
import com.droid.juice.sampleapprest.presentation.view.PhotoListView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoListActivity extends AppCompatActivity implements PhotoListView {

    @BindView(R.id.photo_list_search_view)
    SearchView searchView;

    @BindView(R.id.photo_list_recycler_view)
    RecyclerView photoListView;

    private PhotoListPresenter presenter;
    private PhotoListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);
        ButterKnife.bind(this);
        @SuppressWarnings("UnusedAssignment")
        PhotoListConnector connector = new PhotoListConnector(this);

        presenter.onViewCreate(this);
        setupViews();

    }

    private void setupViews() {
        setupSearchView();
        setupPhotoListView();
    }

    private void setupPhotoListView() {
        adapter = new PhotoListAdapter(this, presenter);

        photoListView.setAdapter(adapter);
        photoListView.setLayoutManager(new GridLayoutManager(this, 2));
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.onSearchClicked(query);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    public void setPresenter(PhotoListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showListLoadErrorMessage() {
        Toast.makeText(this, R.string.photo_list_activity_load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void refreshList() {
        adapter.notifyDataSetChanged();
    }
}
