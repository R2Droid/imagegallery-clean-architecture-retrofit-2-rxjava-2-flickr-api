package com.droid.juice.sampleapprest.data.Flickr;

import com.droid.juice.sampleapprest.domain.entity.Photo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class FlickrPhoto {

    @SerializedName("id")
    @Expose
    private String imageId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("secret")
    @Expose
    private String secret;

    @SerializedName("server")
    @Expose
    private String serverId;

    @SerializedName("farm")
    @Expose
    private int farmId;

    public Photo convertToPhotoEntity() {
        return new Photo(
                title,
                FlickrClient.createImageUrl(farmId, serverId, imageId, secret)
        );
    }
}