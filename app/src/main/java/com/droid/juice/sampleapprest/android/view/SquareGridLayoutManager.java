package com.droid.juice.sampleapprest.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class SquareGridLayoutManager extends RelativeLayout {


    public SquareGridLayoutManager(Context context) {
        super(context);
    }

    public SquareGridLayoutManager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareGridLayoutManager(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}

