package com.droid.juice.sampleapprest.presentation.view;

public interface PhotoListView {
    void showListLoadErrorMessage();

    void refreshList();
}
