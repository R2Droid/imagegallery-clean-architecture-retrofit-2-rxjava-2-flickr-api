package com.droid.juice.sampleapprest.android.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.droid.juice.sampleapprest.R;
import com.droid.juice.sampleapprest.data.util.GlideImageUtil;
import com.droid.juice.sampleapprest.presentation.view.PhotoItemView;

import butterknife.BindView;
import butterknife.ButterKnife;

class PhotoViewHolder extends RecyclerView.ViewHolder implements PhotoItemView {

    @BindView(R.id.item_photo_title)
    TextView titleTextView;

    @BindView(R.id.item_photo_image)
    ImageView imageView;

    public PhotoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void setImage(String url) {
        GlideImageUtil.loadImage(imageView, url);
    }
}
