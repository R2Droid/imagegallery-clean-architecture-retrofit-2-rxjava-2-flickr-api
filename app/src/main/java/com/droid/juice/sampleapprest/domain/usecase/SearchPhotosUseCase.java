package com.droid.juice.sampleapprest.domain.usecase;

import com.droid.juice.sampleapprest.data.EntityGateway;

import io.reactivex.Single;

public class SearchPhotosUseCase implements SingleUseCase {
    private final EntityGateway entityGateway;
    private final String searchText;

    public SearchPhotosUseCase(EntityGateway entityGateway, String searchText) {
        this.entityGateway = entityGateway;
        this.searchText = searchText;
    }

    @Override
    public Single execute() {
        return entityGateway.search(searchText);
    }
}
