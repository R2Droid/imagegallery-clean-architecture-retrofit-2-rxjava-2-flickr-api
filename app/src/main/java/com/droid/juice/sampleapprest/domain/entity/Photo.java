package com.droid.juice.sampleapprest.domain.entity;

public class Photo {

    private final String title;
    private final String image;

    public Photo(String title, String url) {
        this.title = title;
        this.image = url;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }
}
