package com.droid.juice.sampleapprest.presentation.view;

public interface PhotoItemView {

    void setTitle(String title);

    void setImage(String url);
}
