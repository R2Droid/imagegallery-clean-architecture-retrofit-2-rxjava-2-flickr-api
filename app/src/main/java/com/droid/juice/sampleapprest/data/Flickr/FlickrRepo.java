package com.droid.juice.sampleapprest.data.Flickr;

import com.droid.juice.sampleapprest.data.EntityGateway;
import com.droid.juice.sampleapprest.domain.entity.Photo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class FlickrRepo implements EntityGateway {

    private final FlickrService flickrService;

    public FlickrRepo() {
        flickrService = FlickrClient.getFlickrClient().create(FlickrService.class);
    }

    @Override
    public Single search(String searchText) {
        List<Photo> searchResult = new ArrayList<>();

        return Single.create(emitter -> flickrService.getFlickPhotos(searchText)
                .subscribeWith(new SingleObserver<FlickrMetaEntity>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        // do nothing
                    }

                    @Override
                    public void onSuccess(FlickrMetaEntity flickrMetaEntity) {
                        for (FlickrPhoto photo : flickrMetaEntity.getFlickrPhotoPage().getPhotoList()) {
                            searchResult.add(photo.convertToPhotoEntity());
                        }
                        emitter.onSuccess(searchResult);
                    }

                    @Override
                    public void onError(Throwable e) {
                        emitter.onError(e);
                    }
                }));
    }
}
