package com.droid.juice.sampleapprest.data;

import io.reactivex.Single;

public interface EntityGateway<T> {
    Single<T> search(String searchText);
}
