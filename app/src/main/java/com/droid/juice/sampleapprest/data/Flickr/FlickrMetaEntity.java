package com.droid.juice.sampleapprest.data.Flickr;

import com.google.gson.annotations.SerializedName;

class FlickrMetaEntity {
    @SerializedName("stat")
    public String status;

    @SerializedName("photos")
    private FlickrPhotoPage photoPage;

    FlickrPhotoPage getFlickrPhotoPage() {
        return photoPage;
    }
}