package com.droid.juice.sampleapprest.android.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.droid.juice.sampleapprest.R;
import com.droid.juice.sampleapprest.presentation.PhotoListPresenter;

class PhotoListAdapter extends RecyclerView.Adapter<PhotoViewHolder> {
    private final LayoutInflater inflater;
    private final PhotoListPresenter presenter;

    public PhotoListAdapter(Context context, PhotoListPresenter presenter) {
        this.presenter = presenter;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoViewHolder(inflater.inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        presenter.configurePhotoItem(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getPhotoListCount();
    }
}
