package com.droid.juice.sampleapprest.domain.usecase;

import io.reactivex.Single;

interface SingleUseCase<T> {

    Single<T> execute();
}
