package com.droid.juice.sampleapprest.domain.usecase;

import com.droid.juice.sampleapprest.data.EntityGateway;

public class UseCaseFactory {
    private final EntityGateway entityGateway;

    public UseCaseFactory(EntityGateway entityGateway) {
        this.entityGateway = entityGateway;
    }

    public SearchPhotosUseCase SearchPhotosUseCase(String searchText) {
        return new SearchPhotosUseCase(entityGateway, searchText);
    }
}
