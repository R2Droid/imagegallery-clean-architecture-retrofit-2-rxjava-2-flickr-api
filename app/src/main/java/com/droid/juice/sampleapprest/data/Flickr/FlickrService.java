package com.droid.juice.sampleapprest.data.Flickr;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface FlickrService {

    @GET("/services/rest/?method=flickr.photos.search&api_key=223e8cae5910e38f767393f23f8308c8&format=rest&format=json&nojsoncallback=1")
    Single<FlickrMetaEntity> getFlickPhotos(@Query("text") String searchText);
}
