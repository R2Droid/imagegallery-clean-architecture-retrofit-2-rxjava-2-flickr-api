package com.droid.juice.sampleapprest.android.connector;

import com.droid.juice.sampleapprest.android.view.PhotoListActivity;
import com.droid.juice.sampleapprest.data.EntityGateway;
import com.droid.juice.sampleapprest.data.Flickr.FlickrRepo;
import com.droid.juice.sampleapprest.domain.usecase.UseCaseFactory;
import com.droid.juice.sampleapprest.presentation.PhotoListPresenter;

class PhotoListConnector {
    public PhotoListConnector(PhotoListActivity activity) {
        EntityGateway entityGateway = new FlickrRepo();
        UseCaseFactory useCaseFactory = new UseCaseFactory(entityGateway);
        PhotoListPresenter presenter = new PhotoListPresenter(useCaseFactory);

        activity.setPresenter(presenter);
    }
}
